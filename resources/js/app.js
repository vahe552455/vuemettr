
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');
require('./vendor/js/sb-admin.js');
import App from './App';

import navbarComponent from './components/admin/NavbarComponent';
import leftbarComponent from './components/admin/LeftbarComponent';
import login from './components/admin/login/LoginComponent';
import register from './components/admin/register/RegisterComponent';
import user from './components/admin/tables/Users';
import '@fortawesome/fontawesome-free/css/all.css'
require('./../css/sb-admin.css')

// Vue.Component('mycomponent' , require('./'))
Vue.component('navbar-component',navbarComponent);
Vue.component('leftbar-component',leftbarComponent);
Vue.component('login',login);
Vue.component('register',register);

import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [

    {
        path:'/login',
        name:'login',
        component: login,

    },
    {
        path:'/register' ,
        component: register,

    },
    {
        path:'/users' ,
        component: user
    },
/*    {path: '/admin/companies/create', component: CompaniesCreate, name: 'createCompany'},
    {path: '/admin/companies/edit/:id', component: CompaniesEdit, name: 'editCompany'},*/
]
const router = new VueRouter({
    mode:'history',
    routes
})


// Vue.component('example', require('./components/Example.vue'));

/*const app = new Vue({
    el: '#app'
});*//**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    render: h => h(App),
    router
});
