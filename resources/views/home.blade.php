@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div id="app">
             <navbar-component>
             </navbar-component>

                <leftbar-component>
                </leftbar-component>
            </div>
        </div>
    </div>
</div>
@endsection
